#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <initializer_list>
#include <limits>
#include <vector>
#include <tuple>
#include <memory>

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::ofstream;
using std::vector;
using std::tuple;
using std::tie;
using std::make_tuple;
using std::shared_ptr;

#include "io.h"
#include "matrix.h"

#include "MyObject.h"

tuple<int, vector<shared_ptr<IObject>>, Image>
// ???
int binorization(const Image& m)
{
    uint r, g, b;
    RGBApixel p;
   
    int a[255] = {0};
    
    int r, g, b, sum_r = 0, sum_g = 0, sum_b = 0;
    for (uint i = 0; i < size; ++i) {
        for (uint j = 0; j < size; ++j) {
            // Tie is useful for taking elements from tuple
            tie(r, g, b) = m(i, j);
            p.Red = r; p.Green = g; p.Blue = b;
            int bright = 0.3 * r + 0.59* g + 0.11 * b; // узнали яркость по RGB
            
            a[bright]+= 1;
            int max_h = 0, k;
            // ищем ячейку с максимальным значением
            for(uint i = 0; i < 255; i++){
                if (a[i] > max_h;){
                    max_h = a[i];
                    k = 0;
                }
                k +=1; // колличество пикселей с максимальной яркостью
            }
        }
            
        
                
            sum_r += r;
            sum_g += g;
            sum_b += b;
        }
    }
    
    
    auto norm = size * size; // число пикселей
    
    sum_r /= norm;
    sum_g /= norm;
    sum_b /= norm;
    
    return make_tuple(sum_r, sum_g, sum_b);
    
}

// матрица in
repair_mechanism(const Image& in)
{
    
    
    // Base: return array of found objects and index of the correct gear
    // Bonus: return additional parameters of gears
    auto object_array = vector<shared_ptr<IObject>>();
    int result_idx = 0;
    
    
    
    return make_tuple(result_idx, object_array, in.deep_copy());
}

int main(int argc, char **argv)
{
    if (argc != 4)
    {
        cout << "Usage: " << endl << argv[0]
             << " <in_image.bmp> <out_image.bmp> <out_result.txt>" << endl;
        return 0;
    }

    try {
        Image src_image = load_image(argv[1]);
        ofstream fout(argv[3]);

        vector<shared_ptr<IObject>> object_array;
        Image dst_image;
        int result_idx;
        tie(result_idx, object_array, dst_image) = repair_mechanism(src_image);
        save_image(dst_image, argv[2]);

        fout << result_idx << endl;
        fout << object_array.size() << endl;
        for (const auto &obj : object_array)
            obj->Write(fout);

    } catch (const string &s) {
        cerr << "Error: " << s << endl;
        return 1;
    }
}
